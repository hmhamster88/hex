#ifdef GL_ES
precision mediump float;
#endif

//input from vertex shader
varying vec2 v_texCoords;

uniform vec4 u_color;

float border = 0.02;
float radius = 0.5;

void main() {

    vec2 m = v_texCoords - vec2(0.5, 0.5);
    float dist = radius - sqrt(m.x * m.x + m.y * m.y);

    float t = 0.0;
    if (dist > border)
      t = 1.0;
    else if (dist > 0.0)
      t = dist / border;

    gl_FragColor = mix(vec4(0,0,0,0), u_color, t);
}