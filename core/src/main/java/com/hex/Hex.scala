package com.hex

import com.badlogic.gdx.graphics.Texture.TextureFilter
import com.badlogic.gdx.graphics._
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter
import com.badlogic.gdx.graphics.g2d.{TextureAtlas, GlyphLayout, BitmapFont, SpriteBatch}
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.{Vector3, Vector2}
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.{Label, TextButton, Skin}
import com.badlogic.gdx.utils.viewport.{StretchViewport, FitViewport, Viewport}
import com.badlogic.gdx.{Input, Gdx, InputProcessor, ApplicationAdapter}

/**
  * Created by hmhamster on 01.03.16.
  */
class Hex extends ApplicationAdapter with StageManager
{
  val viewSize: Vector2 = new Vector2(640, 512)
  val viewCenter: Vector2 = viewSize.cpy.scl(0.5f)

  var options: HexOptions = null
  var bundle: HexBundle = null

  def restart() =
  {
    field = new HexField(options.fieldSize, options.fieldColors)
  }

  var field: HexField = null

  var batch: SpriteBatch = null
  var shapeRenderer: ShapeRenderer = null
  var textureAtlas: TextureAtlas = null

  var camera: Camera = null
  var viewport: Viewport = null

  var skin: Skin = null
  var _currentStage: Stage = null

  def currentStage_= (stage: Stage) =
  {
    _currentStage = stage
    Gdx.input.setInputProcessor(stage)
  }

  def currentStage = _currentStage

  override def create
  {
    options = new HexOptions
    options.load
    bundle = new HexBundle(options)
    restart()
    batch = new SpriteBatch
    shapeRenderer = new ShapeRenderer()
    camera = new OrthographicCamera
    viewport = new StretchViewport(viewSize.x, viewSize.y, camera)
    viewport.apply
    camera.position.set(viewCenter, 0)

    val generator = new FreeTypeFontGenerator(Gdx.files.internal("Roboto-Regular.ttf"));
    val parameter = new FreeTypeFontParameter();
    parameter.size = (30 * Gdx.graphics.getDensity()).toInt;
    parameter.color = Color.WHITE;
    parameter.characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдежзийклмнопрстуфхцчшщъыьэюя1234567890\"!`?'.,;:()[]{}<>|/@\\^$-%+=#_&~*"
    val font = generator.generateFont(parameter);
    generator.dispose();

    textureAtlas = new TextureAtlas("pack.atlas")
    val textures = textureAtlas.getTextures
    textures.first().setFilter(TextureFilter.Linear, TextureFilter.Linear)
    Meshes.init()
    Shaders.init()

    skin = new Skin();
    skin.addRegions(new TextureAtlas(Gdx.files.internal("uiskin.atlas")));
    skin.add("default-font", font)
    skin.load(Gdx.files.internal("uiskin.json"))

    if(options.locale.isEmpty)
    {
      goToLanguageSelect
    }
    else
    {
      goToMain
    }
  }

  override def render
  {
    camera.update
    Gdx.gl.glClearColor(1, 1, 1, 1)
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

    batch.setProjectionMatrix(camera.combined)
    batch.begin

    batch.end
    if(currentStage != null)
    {
      currentStage.draw()
    }
  }

  override def resize(width: Int, height: Int)
  {
    super.resize(width, height)
    viewport.setWorldSize(width, height)
    viewport.setScreenSize(width, height)
    viewport.apply(true)
  }

  override def goToOptions()
  {
    currentStage = new OptionsStage(viewport, batch, skin, this, options, bundle, textureAtlas)
  }

  override def goToMain
  {
    if(options.needRestart)
    {
      restart()
      options.needRestart = false
    }
    currentStage = new MainStage(viewport, batch, skin, shapeRenderer, this, field, bundle, options, textureAtlas)
  }

  override def goToReset =
  {
    currentStage = new ResetStage(viewport, batch, skin, textureAtlas, this, field, bundle)
  }

  override def goToLanguageSelect =
  {
    currentStage = new LanguageSelectStage(viewport, batch, skin, textureAtlas, this, bundle, options)
  }
}
