package com.hex

import com.badlogic.gdx.graphics.VertexAttributes.Usage
import com.badlogic.gdx.graphics.{Color, VertexAttribute, Mesh}

/**
  * Created by hmhamster on 03.03.16.
  */
class SquareMesh() extends Mesh(true, (2 + 2) * 4, 6,
  new VertexAttribute(Usage.Position, 2, "a_position"),
  new VertexAttribute(Usage.TextureCoordinates, 2, "a_texCoord0"))
{
  setVertices(Array(
    -0.5f, -0.5f,
    1, 1,

    0.5f, -0.5f,
    0, 1,

    0.5f, 0.5f,
    0, 0,

    -0.5f, 0.5f,
    1, 0
  ))

  setIndices(Array[Short](0, 1, 3, 3, 1, 2))
}
