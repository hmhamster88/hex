package com.hex

import com.badlogic.gdx.math.Vector2

/**
  * Created by hmhamster on 29.02.16.
  */
class CubeCords(val x: Int = 0, val y: Int = 0, val z: Int = 0)
{

  def getFlatCoords: Vector2 = new Vector2(x + z / 2.0f, z * (Math.sqrt(3) / 2).toFloat)

  override def equals(o: Any): Boolean =
  {
    if (o == this)
    {
      return true
    }
    if (o.isInstanceOf[CubeCords])
    {
      val other = o.asInstanceOf[CubeCords]
      return x == other.x && y == other.y && z == other.z
    }
    false
  }
}
