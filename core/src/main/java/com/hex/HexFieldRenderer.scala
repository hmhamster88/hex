package com.hex

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.{GlyphLayout, BitmapFont, Batch}
import com.badlogic.gdx.graphics.{GL20, Color}
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.{Matrix4, Vector2}

/**
  * Created by hmhamster on 29.02.16.
  */
class HexFieldRenderer(val renderer: ShapeRenderer)
{
  val maxGemSideCount = 8
  val minGemSideCount = 4

  def drawField(batch: Batch, bitmapFont: BitmapFont, field: HexField, center: Vector2, fieldSize: Float,
                slideDirection: HexRotation, drawNumbers: Boolean)
  {
    val points: Array[HexPoint] = field.points
    val flatCordses: Array[Vector2] = field.flatCodses
    val rowsMap: Array[Array[CubeCords]] = field.rowsMap
    val hexSize: Float = fieldSize * 0.9f / field.height
    renderer.begin(ShapeRenderer.ShapeType.Line)
    if (slideDirection != null)
    {
      renderer.identity
      renderer.setColor(Color.BLACK)
      for(row <- rowsMap)
      {
        val start: Vector2 = slideDirection.rotate(row(0)).getFlatCoords
        val end: Vector2 = slideDirection.rotate(row(row.length - 1)).getFlatCoords
        start.scl(hexSize).add(center)
        end.scl(hexSize).add(center)
        renderer.line(start, end)
      }
    }
    renderer.end

    Gdx.graphics.getGL20().glEnable(GL20.GL_BLEND);
    Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

    Shaders.circleShader.begin
    Shaders.circleShader.projectionMatrix = renderer.getProjectionMatrix
    Shaders.circleShader.color = Color.GRAY

    val pointandCoords = points.zip(flatCordses)

    pointandCoords.foreach
    { case (point, flatCords) =>
      if(point.isEmpty)
      {
        val hexpos: Vector2 = flatCords.cpy.scl(hexSize).add(center)
        val matrix = new Matrix4()
        matrix.trn(hexpos.x, hexpos.y, 0).scl(hexSize / 3)
        Shaders.circleShader.worldMatrix = matrix
        Meshes.sqareMesh.render(Shaders.circleShader, GL20.GL_TRIANGLES, 0, Meshes.sqareMesh.getMaxIndices);
      }
    }
    Shaders.circleShader.end

    batch.setColor(Color.RED)

    renderer.begin(ShapeRenderer.ShapeType.Filled)

    val gemSize: Float = hexSize / 1.5f
    val lightAngle: Float = Math.toRadians(333).toFloat

    pointandCoords.foreach
    { case (point, flatCords) =>
      if (!point.isEmpty)
      {
        val hexpos: Vector2 = flatCords.cpy.scl(hexSize).add(center)
        drawGem(point.numberValue, gemSize, hexpos, point.colorValue, lightAngle)
      }
    }
    renderer.end

    if(drawNumbers)
    {
      batch.begin()
      bitmapFont.setColor(Color.WHITE)
      val layout = new GlyphLayout();
      pointandCoords.foreach
      {
        case (point, flatCords) =>
          if (!point.isEmpty)
          {
            val hexpos: Vector2 = flatCords.cpy.scl(hexSize).add(center)
            val text = (point.numberValue).toString
            layout.setText(bitmapFont, text)
            bitmapFont.draw(batch, text, hexpos.x - layout.width / 2, hexpos.y + layout.height / 2)
          }
      }
      batch.end()
    }
  }

  private def drawGem(numValue: Int, size: Float, position: Vector2, color: Color, lightAngle: Float)
  {
    val levelCount: Int = getGemLevelCount(numValue)
    val sideCount: Int = getGemSideCount(numValue)
    val sizeStep: Float = size / levelCount

    for(level <- 0 to levelCount)
    {
        drawGemLevel(sideCount, size - sizeStep * level, position, color, lightAngle - (level % 2) * Math.PI.toFloat)
    }

  }

  private def getGemSideCount(numberValue: Int): Int =
  {
    return minGemSideCount + (numberValue - 1) % (maxGemSideCount - minGemSideCount + 1)
  }

  private def getGemLevelCount(numberValue: Int): Int =
  {
    return 1 + (numberValue - 1) / (maxGemSideCount - minGemSideCount + 1)
  }

  private def drawGemLevel(count: Int, size: Float, position: Vector2, color: Color, lightAngle: Float)
  {
    renderer.identity
    renderer.translate(position.x, position.y, 0)
    renderer.scale(size / 2, size / 2, 0)
    val partAngle: Float = Math.PI.toFloat * 2 / count
    var highlight: Boolean = false

    for(i <- 0 until count)
    {
        val angle0: Float = i * partAngle
        val angle1: Float = (i + 1) * partAngle
        val centerAngle: Float = angle0 + partAngle / 2
        var x: Float = Math.abs(lightAngle - centerAngle)
        if (x > Math.PI)
        {
          x = Math.PI.toFloat - (x - Math.PI.toFloat)
        }
        var c: Float = -(x / Math.PI).toFloat * 0.6f
        if (c < -0.5f && !highlight)
        {
          highlight = true
          c = 0.2f
        }
        renderer.setColor(new Color(color.r + c, color.g + c, color.b + c, color.a))
        renderer.triangle(Math.cos(angle0).toFloat, Math.sin(angle0).toFloat,
          Math.cos(angle1).toFloat, Math.sin(angle1).toFloat,
          0, 0)
    }

  }
}

