//our attributes
attribute vec2 a_position;
attribute vec4 a_color;
attribute vec2 a_texCoord0;

//our camera matrix
uniform mat4 u_projTrans;
// world Transform Matrix
uniform mat4 u_worldTrans;

//send the color out to the fragment shader
varying vec2 v_texCoords;

void main() {
    //v_color = a_color;
    v_texCoords = a_texCoord0;
    gl_Position = u_projTrans * u_worldTrans * vec4(a_position.xy, 0.0, 1.0);
}