package com.hex

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.{GL20, Color}
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion
import com.badlogic.gdx.graphics.g2d.{SpriteBatch, TextureRegion, Batch}
import com.badlogic.gdx.math.{Matrix4, Vector2}
import com.badlogic.gdx.scenes.scene2d.ui.{Button, Skin}

/**
  * Created by hmhamster on 18.03.16.
  */
class CircleButton(val atlasRegion: AtlasRegion,
                   val size:Float = Gdx.graphics.getDensity() * 64) extends Button
{
  val radius = 0.4f
  val shadowShift = new Vector2(0.0f, -0.06f)
  val pressedShadowShift = new Vector2(0.0f, -0.03f)
  val buttonColor = new Color(0x2196F3FF)
  val iconScale = 0.7f;

  override def getPrefWidth(): Float =
  {
    return size;
  }

  override def getPrefHeight(): Float =
  {
    return size;
  }

  override def draw(batch: Batch, parentAlpha: Float): Unit =
  {
    batch.end()
    Gdx.graphics.getGL20().glEnable(GL20.GL_BLEND);
    Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
    Shaders.circleButtonShader.begin
    Shaders.circleButtonShader.color = buttonColor
    Shaders.circleButtonShader.projectionMatrix = batch.getProjectionMatrix
    Shaders.circleButtonShader.buttonRadius = radius
    Shaders.circleButtonShader.shadowShift = if(isPressed) pressedShadowShift else shadowShift

    val width = getWidth / (radius * 2)
    val height = getHeight / (radius * 2)

    val shift = if(isPressed) shadowShift.cpy().sub(pressedShadowShift) else new Vector2()
    val yShift = shift.y * height
    val xShift = shift.x * width

    Shaders.circleButtonShader.border = 2 / width

    Shaders.circleButtonShader.worldMatrix =  new Matrix4().trn(getX +  getWidth / 2 + xShift,
      getY + getHeight / 2.0f + yShift, 0).scl(width)

    Meshes.sqareMesh.render(Shaders.circleButtonShader, GL20.GL_TRIANGLES, 0, Meshes.sqareMesh.getMaxIndices);

    Shaders.circleButtonShader.end()

    batch.begin()
    batch.setColor(Color.WHITE)
    batch.draw(atlasRegion, getX + (getWidth - width * iconScale) / 2.0f + xShift,
      getY + (getHeight - height * iconScale) / 2.0f + yShift, width * iconScale, height * iconScale)
  }
}
