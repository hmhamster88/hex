package com.hex

import java.util.Locale

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.utils.I18NBundle

/**
  * Created by hmhamster on 09.03.16.
  */
class HexBundle(val hexOptions: HexOptions)
{
  val file = Gdx.files.internal("HexBundle")
  var bundle: I18NBundle = null

  load

  def load: Unit =
  {
    val locale = new Locale(if(hexOptions.locale.isEmpty) hexOptions.allLocales.head._1 else hexOptions.locale)
    bundle = I18NBundle.createBundle(file, locale)
  }

  def score(value: Int) = bundle.format("score", value.asInstanceOf[AnyRef])

  def reset = bundle.get("reset")
  def options = bundle.get("options")
  def gameOver = bundle.get("gameOver")
  def yes = bundle.get("yes")
  def no = bundle.get("no")
  def resetQuestion = bundle.get("resetQuestion")
  def fieldSize = bundle.get("fieldSize")
  def colorsCount = bundle.get("colorsCount")
  def back = bundle.get("back")
  def language = bundle.get("language")
  def showValues = bundle.get("showValues")

}
