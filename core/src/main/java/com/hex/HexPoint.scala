package com.hex

import java.util.Random

import com.badlogic.gdx.graphics.Color

/**
  * Created by hmhamster on 01.03.16.
  */
class HexPoint
{
  val random: Random = new Random
  val emptyColor: Color = Color.GRAY

  var colorValue: Color = emptyColor
  var numberValue: Int = 0

  resetValue


  def isEmpty: Boolean =
  {
    numberValue == 0
  }

  def resetValue
  {
    numberValue = 0
    colorValue = emptyColor
  }

  def swapValues(other: HexPoint)
  {
    val tmp = numberValue
    this.numberValue  = other.numberValue
    other.numberValue = tmp
    val tmpColor = colorValue
    colorValue = other.colorValue
    other.colorValue = tmpColor
  }

  def addValueFrom(other: HexPoint)
  {
    numberValue += 1
    other.resetValue
  }

  def newRandomValue(availableColors: Array[Color])
  {
    numberValue = if (random.nextFloat > 0.8f) 2 else 1
    colorValue = availableColors(random.nextInt(availableColors.length))
  }

  override def toString: String =
  {
    return Integer.toString(numberValue)
  }

  override def equals(obj: Any): Boolean =
  {
    if (obj.isInstanceOf[HexPoint])
    {
      val other: HexPoint = obj.asInstanceOf[HexPoint]
      return numberValue == other.numberValue && (colorValue eq other.colorValue)
    }
    return false
  }
}
