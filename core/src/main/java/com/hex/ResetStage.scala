package com.hex

import com.badlogic.gdx.graphics.g2d.{TextureAtlas, Batch}
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.scenes.scene2d.{InputEvent, Stage}
import com.badlogic.gdx.scenes.scene2d.ui.{Label, TextButton, Table, Skin}
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.viewport.Viewport

/**
  * Created by HMHamster on 08.03.2016.
  */
class ResetStage(viewport: Viewport, batch: Batch, val skin: Skin, val textureAtlas: TextureAtlas,
                 val stageManager: StageManager, val field: HexField,
                 val hexBundle: HexBundle) extends Stage(viewport, batch)
{
  val table = new Table()
  table.setFillParent(true)

  val label = new Label(hexBundle.resetQuestion, skin)
  val yesButton = new CircleButton(textureAtlas.findRegion("check"))
  val noButton = new CircleButton(textureAtlas.findRegion("close"))

  label.setAlignment(Align.center)

  table.add(label).pad(8).colspan(2)
  table.row()
  table.add(yesButton).pad(8)
  table.add(noButton).pad(8)

  addActor(table)

  yesButton.addListener(new ClickListener()
  {
    override def clicked(event: InputEvent, x: Float, y: Float) =
    {
      field.reset
      stageManager.goToMain
    }
  }
  )

  noButton.addListener(new ClickListener()
  {
    override def clicked(event: InputEvent, x: Float, y: Float) =
    {
      stageManager.goToMain
    }
  }
  )
}
