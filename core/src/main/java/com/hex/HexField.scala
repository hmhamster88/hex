package com.hex

import java.util.Random

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Vector2

import scala.util.control.Breaks._

/**
  * Created by hmhamster on 01.03.16.
  */
class HexField(val size: Int, val availableColors: Array[Color])
{
  val count = 3 * size * (size - 1) + 1
  val height: Int = size * 2 - 1

  val points = (for (i <- 0 until count) yield new HexPoint).toArray
  val flatCodses: Array[Vector2] = new Array[Vector2](count)

  reset

  val cubeMap = Array.ofDim[HexPoint](height, height, height)
  var rowsMap: Array[Array[CubeCords]] = new Array[Array[CubeCords]](height)

  var score: Int = 0

  {
    var rowSize: Int = size
    var num: Int = 0
    val max: Int = size - 1
    var startX: Int = 0
    var startY: Int = max

    for (row <- 0 until height)
    {

      val cubeZ: Int = row - max
      var cubeX: Int = startX
      var cubeY: Int = startY
      rowsMap(row) = new Array[CubeCords](rowSize)

      for (column <- 0 until rowSize)
      {
        cubeMap(cubeX + max)(cubeY + max)(cubeZ + max) = points(num)
        val cubeCords: CubeCords = new CubeCords(cubeX, cubeY, cubeZ)
        rowsMap(row)(column) = cubeCords
        flatCodses(num) = cubeCords.getFlatCoords
        cubeX += 1
        cubeY -= 1
        num += 1

      }
      if (row < max)
      {
        startX -= 1
        rowSize += 1
      }
      else
      {
        startY -= 1
        rowSize -= 1
      }
    }
  }


  def reset
  {
    val random: Random = new Random
    for (point <- points)
    {
      point.resetValue
    }
    points(random.nextInt(points.length)).newRandomValue(availableColors)
    score = 0
  }

  def getPoint(coords: CubeCords): HexPoint =
  {
    val l: Int = size - 1
    cubeMap(coords.x + l)(coords.y + l)(coords.z + l)
  }

  def getRotatedRows(rotation: HexRotation): Array[Array[HexPoint]] =
  {
    val result: Array[Array[HexPoint]] = new Array[Array[HexPoint]](height)
    for (row <- 0 until height)
    {
      val rowSize: Int = rowsMap(row).length
      result(row) = new Array[HexPoint](rowSize)
      for (column <- 0 until rowSize)
      {
        var coords: CubeCords = rowsMap(row)(column)
        coords = rotation.rotate(coords)
        result(row)(column) = getPoint(coords)
      }
    }
    return result
  }

  def step(rotation: HexRotation): Boolean =
  {
    var rows: Array[Array[HexPoint]] = getRotatedRows(rotation)
    if (foldRows(rows, false))
    {
      val emptyPoints = points.filter(p => p.isEmpty);
      if (!emptyPoints.isEmpty)
      {
        val random: Random = new Random
        emptyPoints(random.nextInt(emptyPoints.size)).newRandomValue(availableColors)
      }
    }
    for (rotation <- HexRotationValues.list)
    {
      rows = getRotatedRows(rotation)
      if (foldRows(rows, true))
      {
        return true
      }
    }
    return false
  }

  def foldRows(points: Array[Array[HexPoint]], onlyCheck: Boolean): Boolean =
  {
    var moved: Boolean = false
    for (row <- points)
    {
      if (foldRow(row, onlyCheck))
      {
        if (onlyCheck)
        {
          return true
        }
        moved = true;
      }
    }
    return moved
  }

  def foldRow(row: Array[HexPoint], onlyCheck: Boolean): Boolean =
  {
    var moved: Boolean = false
    var i: Int = row.length - 1;

    while (i >= 0)
    {
      breakable
      {
        for (j <- i - 1 to 0 by -1)
        {
          if (!row(j).isEmpty)
          {
            if (row(i).isEmpty)
            {
              if (onlyCheck)
              {
                return true
              }
              row(i).swapValues(row(j))
              moved = true
            }
            else if (row(i) == row(j))
            {
              if (onlyCheck)
              {
                return true
              }
              score += row(i).numberValue * 2
              row(i).addValueFrom(row(j))
              moved = true
              i -= 1
              break
            }
            else
            {
              break
            }
          }
          else if (onlyCheck)
          {
            return true
          }
        }
      }
      i -= 1;
    }

    return moved
  }
}
