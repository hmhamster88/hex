package com.hex

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.ui._
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener

/**
  * Created by HMHamster on 08.03.2016.
  */
class Spinner[A](skin: Skin, val textureAtlas: TextureAtlas, val indexedSeq: IndexedSeq[A],
                 selectedValue: A, val looped: Boolean = false) extends Table(skin)
{
  val size =  Gdx.graphics.getDensity() * 48
  val minusButton = new CircleButton(textureAtlas.findRegion(if(looped) "left" else "minus"), size)
  val plusButton = new CircleButton(textureAtlas.findRegion(if(looped) "right" else "plus"), size)
  val label = new Label(value.toString, skin)
  var _selectedIndex = 0

  value = selectedValue

  def selectedIndex = _selectedIndex
  def selectedIndex_=(newIndex: Int) =
  {
    if(looped)
    {
      var index  = newIndex
      if(index < 0)
      {
        index = indexedSeq.length - 1
      }
      else if(index >= indexedSeq.length)
      {
        index = 0
      }
      _selectedIndex = index
    }
    else if(newIndex >= 0 && newIndex < indexedSeq.length)
    {
      _selectedIndex = newIndex
    }
    label.setText(value.toString)
  }

  def value = indexedSeq(selectedIndex)
  def value_=(newValue: A) =
  {
    val newIndex = indexedSeq.indexOf(newValue)
    if(newIndex > 0)
    {
      selectedIndex = newIndex
      label.setText(newValue.toString)
    }

  }

  add(minusButton)
  add(label).pad(8).expand()
  add(plusButton)

  minusButton.addListener(new ClickListener()
  {
    override def clicked(event: InputEvent, x: Float, y: Float): Unit =
    {
        selectedIndex -= 1
    }
  }
  )

  plusButton.addListener(new ClickListener()
  {
    override def clicked(event: InputEvent, x: Float, y: Float): Unit =
    {
        selectedIndex += 1
    }
  }
  )
}
