package com.hex

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color

/**
  * Created by hmhamster on 04.03.16.
  */
class HexOptions()
{
  val allLocales = Map("en" -> "English", "ru" -> "Русский")
  val allFieldColors = Array(Color.RED, Color.BLUE, Color.GREEN, Color.MAGENTA)
  var fieldSize = 3
  var colorsCount = 2
  var locale = ""
  var showValues = true
  def fieldColors = allFieldColors.take(colorsCount)
  var needRestart = false

  val prefs = Gdx.app.getPreferences("Hex")

  def load =
  {
    fieldSize = prefs.getInteger("fieldSize", 3)
    colorsCount = prefs.getInteger("colorsCount", 2)
    locale = prefs.getString("locale", "")
    showValues = prefs.getBoolean("showValues", true)
  }

  def save =
  {
    prefs.putInteger("fieldSize", fieldSize)
    prefs.putInteger("colorsCount", colorsCount)
    prefs.putString("locale", locale)
    prefs.putBoolean("showValues", showValues)
    prefs.flush()
  }

}
