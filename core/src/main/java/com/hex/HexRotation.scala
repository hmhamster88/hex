package com.hex

import com.badlogic.gdx.math.Vector2

/**
  * Created by hmhamster on 01.03.16.
  */
sealed abstract class HexRotation(val angle: Int)
{
  def rotate(cubeCords: CubeCords): CubeCords
}

object HexRotationValues
{
  case object A0 extends HexRotation(0)
  {
    override def rotate(cubeCords: CubeCords): CubeCords =
      new CubeCords(cubeCords.x, cubeCords.y, cubeCords.z)
  }
  case object A60 extends HexRotation(60)
  {
    override def rotate(cubeCords: CubeCords): CubeCords =
      new CubeCords(-cubeCords.z, -cubeCords.x, -cubeCords.y)
  }
  case object A120 extends HexRotation(120)
  {
    override def rotate(cubeCords: CubeCords): CubeCords =
      new CubeCords(cubeCords.y, cubeCords.z, cubeCords.x)
  }
  case object A180 extends HexRotation(180)
  {
    override def rotate(cubeCords: CubeCords): CubeCords =
      new CubeCords(-cubeCords.x, -cubeCords.y, -cubeCords.z)
  }
  case object A240 extends HexRotation(240)
  {
    override def rotate(cubeCords: CubeCords): CubeCords =
      new CubeCords(cubeCords.z, cubeCords.x, cubeCords.y)
  }
  case object A300 extends HexRotation(300)
  {
    override def rotate(cubeCords: CubeCords): CubeCords =
      new CubeCords(-cubeCords.y, -cubeCords.z, -cubeCords.x)
  }

  val list = List(A0, A60, A120, A180, A240, A300)

  def fromVector(vector2: Vector2): HexRotation =
  {
    return fromDegrees(vector2.angle)
  }

  def fromDegrees(degrees: Double): HexRotation =
  {
    val iAngle: Long = (if (degrees < 0) degrees + 360 else degrees / 60.0).round * 60
    iAngle match
    {
      case 60 => A60
      case 120 => A120
      case 180 => A180
      case 240 => A240
      case 300 => A300
      case 360 => A0
      case _ => A0
    }
  }
}
