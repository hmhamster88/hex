#ifdef GL_ES
    precision mediump float;
#endif

varying vec2 v_texCoords;

uniform vec4 u_color;
uniform float u_button_radius;
uniform vec2 u_shadow_shift;
uniform float u_border;

float u_icon_scale = 0.7;
vec4 u_shadow_color = vec4(0, 0, 0, 0.3);

float shadow_border = length(u_shadow_shift) * 2.0;


float smoothstep(float edge0, float edge1, float x)
{
    float t;
    t = clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0);
    return t * t * (3.0 - 2.0 * t);
}

vec4 alphaBlend(vec4 src, vec4 dst)
{
    float outa = src.a + dst.a * (1.0 - src.a);
    if(outa == 0.0)
    {
        return vec4(0.0,0.0,0.0,0.0);
    }
    return vec4((src.rgb * src.a + dst.rgb * dst.a * (1.0 - src.a)) / outa, outa);
}

void main()
{
float shadow_radius = u_button_radius - length(u_shadow_shift);

    vec2 distance_from_center = v_texCoords - vec2(0.5, 0.5);

    vec2 m = distance_from_center;
    float dist = sqrt(m.x * m.x + m.y * m.y);

    vec2 shadow_m = distance_from_center + u_shadow_shift;
    float shadow_dist = sqrt(shadow_m.x * shadow_m.x + shadow_m.y * shadow_m.y);

    vec4 background = vec4(1.0, 1.0, 1.0, 0.0);

    vec4 shadow_out_color = u_shadow_color;

    if (shadow_dist < shadow_radius)
    {
        shadow_out_color = u_shadow_color;
    }
    else
    {
        shadow_out_color = mix(u_shadow_color, vec4(u_shadow_color.rgb, 0.0),
            smoothstep(0.0, 1.0, (shadow_dist - shadow_radius) / shadow_border));
    }

    vec4 out_color = shadow_out_color;

    if (dist < u_button_radius)
    {
        out_color = u_color;
    }
    else if( dist < u_button_radius + u_border)
    {
        vec4 col = vec4(u_color.rgb, u_color.a * smoothstep(1.0, 0.0,(dist - u_button_radius) / u_border));
        out_color = alphaBlend(col, out_color);
    }
    gl_FragColor = out_color;
}
