package com.hex

/**
  * Created by hmhamster on 04.03.16.
  */
trait StageManager
{
  def goToOptions
  def goToMain
  def goToReset
  def goToLanguageSelect
}
