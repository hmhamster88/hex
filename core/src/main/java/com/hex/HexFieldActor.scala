package com.hex

import com.badlogic.gdx.graphics.g2d.{BitmapFont, Batch}
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.{Table, Skin}

/**
  * Created by hmhamster on 02.03.16.
  */
class HexFieldActor(var hexField: HexField, val skin:Skin, val shapeRenderer: ShapeRenderer,
                    val hexOptions: HexOptions) extends Table
{
  var slideDirection: HexRotation = null
  val hexFieldRenderer = new HexFieldRenderer(shapeRenderer)

  override def draw(batch: Batch, parentAlpha: Float): Unit =
  {
    super.draw(batch, parentAlpha)
    batch.end()
    shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix)
    val fieldSize = if(getHeight > getWidth) getWidth else getHeight
    hexFieldRenderer.drawField(batch, skin.getFont("default-font"), hexField,
      new Vector2( getX + getWidth / 2, getY + getHeight / 2), fieldSize, slideDirection, hexOptions.showValues)
    shapeRenderer.identity()
    batch.begin()
  }
}
