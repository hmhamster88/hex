package com.hex

import com.badlogic.gdx.graphics.Texture.TextureFilter
import com.badlogic.gdx.{Gdx, Input}
import com.badlogic.gdx.graphics.{Texture, Color}
import com.badlogic.gdx.graphics.g2d.{TextureAtlas, TextureRegion, Batch}
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.{Vector3, Vector2}
import com.badlogic.gdx.scenes.scene2d.{InputEvent, Stage}
import com.badlogic.gdx.scenes.scene2d.ui.{Table, Label, TextButton, Skin}
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.viewport.Viewport

/**
  * Created by hmhamster on 02.03.16.
  */
class MainStage(viewport: Viewport, batch: Batch, val skin: Skin, val shapeRenderer: ShapeRenderer,
                val stageManager: StageManager, val field: HexField, val hexBundle: HexBundle,
                val hexOptions: HexOptions, var textureAtlas: TextureAtlas) extends Stage(viewport, batch)
{
  var touchDownPoint: Vector2 = null
  var gameOver = false

  val width = viewport.getWorldWidth
  val height = viewport.getWorldHeight

  val table = new Table()
  table.setFillParent(true)

  val hexFieldActor = new HexFieldActor(field, skin, shapeRenderer, hexOptions)
  hexFieldActor.setFillParent(true)

  val gameOverLabel = new Label("", skin)

  val dpi = Gdx.graphics.getDensity()

  val optionsButton = new CircleButton(textureAtlas.findRegion("settings"));

  optionsButton.addListener(new ClickListener()
  {
    override def clicked(event: InputEvent, x: Float, y: Float) = stageManager.goToOptions
  }
  )

  val restartButton = new CircleButton(textureAtlas.findRegion("refresh"));
  restartButton.addListener(new ClickListener()
  {
    override def clicked(event: InputEvent, x: Float, y: Float) =
    {
      if (gameOver)
      {
        gameOver = false
        gameOverLabel.setText("")
        field.reset
      }
      else
      {
        stageManager.goToReset
      }
    }
  }
  )


  val scoreLabel = new Label(hexBundle.score(0), skin)

  table.add(scoreLabel).expandX().left().pad(dpi * 12)
  table.row()
  table.add(gameOverLabel).expand().center().colspan(2)
  table.row()
  table.add(restartButton).left().pad(dpi * 12)
  table.add(optionsButton).right().pad(dpi * 12)

  addActor(hexFieldActor)
  addActor(table)

  def getSlideRotation(keycode: Int): HexRotation =
    keycode match
    {
      case Input.Keys.D => HexRotationValues.A0
      case Input.Keys.E => HexRotationValues.A60
      case Input.Keys.W => HexRotationValues.A120
      case Input.Keys.A => HexRotationValues.A180
      case Input.Keys.Z => HexRotationValues.A240
      case Input.Keys.X => HexRotationValues.A300
      case _ => return null
    }

  override def keyDown(keycode: Int): Boolean =
  {
    if(super.keyDown(keycode))
    {
      return true
    }
    val rotation: HexRotation = getSlideRotation(keycode)
    if (rotation != null)
    {
      hexFieldActor.slideDirection = rotation
      return true
    }
    return false
  }

  override def keyUp(keycode: Int): Boolean =
  {
    if(super.keyUp(keycode))
    {
      return true;
    }
    val rotation: HexRotation = getSlideRotation(keycode)
    if (rotation != null)
    {
      step(rotation)
      hexFieldActor.slideDirection = null
      return true
    }
    return false
  }
  override def touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean =
  {
    if(super.touchDown(screenX, screenY, pointer, button))
    {
      return true;
    }
    touchDownPoint = toViewCoords(screenX, screenY)
    return true
  }

  def toViewCoords(screenX: Int, screenY: Int): Vector2 =
  {
    val worldCoordinates: Vector3 = viewport.unproject(new Vector3(screenX, screenY, 0))
    return new Vector2(worldCoordinates.x, worldCoordinates.y)
  }

  override def touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean =
  {
    if(super.touchUp(screenX, screenY, pointer, button))
    {
      return true;
    }
    val rotation: HexRotation = getSlideRotation(screenX, screenY)
    if (rotation != null)
    {
      step(rotation)
      hexFieldActor.slideDirection = null
    }
    return true
  }

  def getSlideRotation(screenX: Int, screenY: Int): HexRotation =
  {
    if (touchDownPoint != null)
    {
      val touchUpPoint: Vector2 = toViewCoords(screenX, screenY)
      val moveVector: Vector2 = touchUpPoint.cpy.sub(touchDownPoint)
      if (moveVector.len > width / 8)
      {
        return HexRotationValues.fromVector(moveVector)
      }
    }
    return null
  }

  override def touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean =
  {
    if(super.touchDragged(screenX, screenY, pointer))
    {
      return true;
    }
    val rotation: HexRotation = getSlideRotation(screenX, screenY)
    hexFieldActor.slideDirection = rotation
    return false
  }


  private def step(rotation: HexRotation)
  {
    if (!gameOver)
    {
      gameOver = !field.step(rotation)
      if(gameOver)
      {
        gameOverLabel.setText("Game Over.")
      }
      scoreLabel.setText(hexBundle.score(field.score))
    }
  }
}
