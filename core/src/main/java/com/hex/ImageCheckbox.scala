package com.hex

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.{Batch, TextureRegion}
import com.badlogic.gdx.scenes.scene2d.ui.Button

/**
  * Created by HMHamster on 19.03.2016.
  */
class ImageCheckbox(val imageBlank: TextureRegion, val imageMarked: TextureRegion,
                    val size:Float = Gdx.graphics.getDensity() * 48) extends  Button
{
  override def getPrefWidth(): Float =
  {
    return size;
  }

  override def getPrefHeight(): Float =
  {
    return size;
  }
  override def draw(batch: Batch, parentAlpha: Float): Unit =
  {
    val image =
    if(isChecked)
    {
      imageMarked
    }
    else
    {
      imageBlank
    }
    batch.setColor(getColor)
    batch.draw(image, getX, getY, getWidth, getHeight)
  }
}
