package com.hex

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.{TextureAtlas, Batch}
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.scenes.scene2d.{InputEvent, Stage}
import com.badlogic.gdx.scenes.scene2d.ui._
import com.badlogic.gdx.scenes.scene2d.utils.{TextureRegionDrawable, ClickListener}
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.viewport.Viewport

/**
  * Created by hmhamster on 04.03.16.
  */
class OptionsStage(viewport: Viewport, batch: Batch, val skin: Skin,
                   stageManager: StageManager, val options: HexOptions,
                   val hexBundle: HexBundle, var textureAtlas: TextureAtlas) extends Stage(viewport, batch)
{
  var bigTable = new Table()
  bigTable.setFillParent(true)
  val table = new Table()

  val languageLabel = new Label(hexBundle.language, skin)
  val languageSpinner = new Spinner[String](skin, textureAtlas, options.allLocales.values.toIndexedSeq,
    options.allLocales(options.locale), true)

  val showValuesLabel = new Label(hexBundle.showValues, skin)
  val showValuesCheck = new ImageCheckbox(textureAtlas.findRegion("checkbox-blank"),
    textureAtlas.findRegion("checkbox-marked"))
  showValuesCheck.setChecked(options.showValues)
  showValuesCheck.setColor(Color.GRAY)

  val fieldSizeLabel = new Label(hexBundle.fieldSize, skin)
  val colorsCountLabel = new Label(hexBundle.colorsCount, skin)
  colorsCountLabel.setAlignment(Align.center)
  val fieldSizeSpinner = new Spinner[Int](skin, textureAtlas, 3 to 6, options.fieldSize)
  val colorsCountSpinner = new Spinner(skin, textureAtlas, 2 to options.allFieldColors.length, options.colorsCount)

  val backButton = new CircleButton(textureAtlas.findRegion("check"))
  backButton.pad(12)

  backButton.addListener(new ClickListener()
  {
    override def clicked(event: InputEvent, x: Float, y: Float): Unit =
    {
      val needRestart = fieldSizeSpinner.value != options.fieldSize || colorsCountSpinner.value != options.colorsCount
      options.needRestart = needRestart
      options.fieldSize = fieldSizeSpinner.value
      options.colorsCount = colorsCountSpinner.value
      options.locale = options.allLocales.find(_._2 == languageSpinner.value).getOrElse(options.allLocales.head)._1
      options.showValues = showValuesCheck.isChecked
      hexBundle.load
      options.save
      stageManager.goToMain
    }
  }
  )
  val pad = 12 * Gdx.graphics.getDensity()
  table.add(languageLabel).pad(pad)
  table.add(languageSpinner).pad(pad).fill()
  table.row()
  table.add(showValuesLabel).pad(pad)
  table.add(showValuesCheck).pad(pad)
  table.row()
  table.add(fieldSizeLabel).pad(pad)
  table.add(fieldSizeSpinner).pad(pad).fill().width(300 * Gdx.graphics.getDensity())
  table.row()
  table.add(colorsCountLabel).pad(pad)
  table.add(colorsCountSpinner).pad(pad).fill()

  bigTable.add(table).expand()
  bigTable.row()
  bigTable.add(backButton).pad(pad).right()

  this.addActor(bigTable)
}
