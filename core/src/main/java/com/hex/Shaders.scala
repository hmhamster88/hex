package com.hex

/**
  * Created by HMHamster on 19.03.2016.
  */
object Shaders
{
  private var _circleShader: CircleShader = null
  private var _circleButtonShader: CircleButtonShader = null

  def circleShader = _circleShader
  def circleButtonShader = _circleButtonShader

  def init() =
  {
    _circleShader = new CircleShader()
    _circleButtonShader = new CircleButtonShader()
  }
}
