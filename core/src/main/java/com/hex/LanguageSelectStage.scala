package com.hex

import com.badlogic.gdx.graphics.g2d.{TextureAtlas, Batch}
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.scenes.scene2d.{InputEvent, Stage}
import com.badlogic.gdx.scenes.scene2d.ui.{Label, TextButton, Table, Skin}
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.viewport.Viewport

/**
  * Created by hmhamster on 09.03.16.
  */
class LanguageSelectStage(viewport: Viewport, batch: Batch, val skin: Skin, var textureAtlas: TextureAtlas,
                          val stageManager: StageManager, val hexBundle: HexBundle, val hexOptions: HexOptions) extends Stage(viewport, batch)
{
  val table = new Table()
  table.setFillParent(true)

  for((locale, localeName) <- hexOptions.allLocales)
  {
    val button = new CircleButton(textureAtlas.findRegion("right"))
    button.pad(12)
    button.addListener(new ClickListener()
    {
      override def clicked(event: InputEvent, x: Float, y: Float) =
      {
        hexOptions.locale = locale
        hexOptions.save
        hexBundle.load
        stageManager.goToMain
      }
    })
    table.add(new Label(localeName, skin)).pad(8).center()
    table.add(button).pad(8).fillX()
    table.row()
  }

  addActor(table)
}
