package com.hex

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.glutils.ShaderProgram
import com.badlogic.gdx.math.Matrix4

/**
  * Created by hmhamster on 03.03.16.
  */
class CircleShader extends  ShaderProgram(Gdx.files.internal("CircleShader.vert"), Gdx.files.internal("CircleShader.frag"))
{
  if (!isCompiled())
  {
    Gdx.app.error("Circle Shader Error", "Shader compilation failed:\n" + super.getLog());
  }

  var _projectionMatrix = new Matrix4()
  var _worldMatrix = new Matrix4()
  var _color = Color.BLACK

  def projectionMatrix_= (value: Matrix4) =
  {
    super.setUniformMatrix("u_projTrans", value)
    _projectionMatrix = value
  }

  def projectionMatrix = _projectionMatrix

  def worldMatrix_=(value: Matrix4) =
  {
    super.setUniformMatrix("u_worldTrans", value)
    _worldMatrix = value
  }

  def worldMatrix = _worldMatrix

  def color_= (value: Color) =
  {
    super.setUniformf("u_color", value)
    _color = value
  }

  def color = _color
}
