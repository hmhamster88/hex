package com.hex

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.glutils.ShaderProgram
import com.badlogic.gdx.math.{Matrix4, Vector2}

/**
  * Created by hmhamster on 16.03.16.
  */
class CircleButtonShader extends  ShaderProgram(Gdx.files.internal("CircleButtonShader.vert"),
  Gdx.files.internal("CircleButtonShader.frag"))
{
  if (!isCompiled())
  {
    Gdx.app.error("Circle Shader Error", "Shader compilation failed:\n" + super.getLog());
  }

  var _buttonRadius = 0.4f
  def buttonRadius_=(value: Float): Unit =
  {
      super.setUniformf("u_button_radius", value)
    _buttonRadius = value;
  }
  def buttonRadius = _buttonRadius

  var _shadowShift = new Vector2(0.0f, -0.06f)
  def shadowShift_=(value: Vector2): Unit  =
  {
    super.setUniformf("u_shadow_shift",value)
    _shadowShift = value
  }

  def shadowShift = _shadowShift

  var _border = 0.008f
  def border_=(value: Float)=
  {
    super.setUniformf("u_border",value)
    _border = value
  }
  def border = _border

  var _worldMatrix = new Matrix4()
  def worldMatrix_=(value: Matrix4) =
  {
    super.setUniformMatrix("u_worldTrans", value)
    _worldMatrix = value
  }
  def worldMatrix = _worldMatrix

  var _projectionMatrix = new Matrix4()
  def projectionMatrix_= (value: Matrix4) =
  {
    super.setUniformMatrix("u_projTrans", value)
    _projectionMatrix = value
  }
  def projectionMatrix = _projectionMatrix

  var _color = Color.BLACK
  def color_= (value: Color) =
  {
    super.setUniformf("u_color", value)
    _color = value
  }
  def color = _color
}
